/**
 * Created by César Gutiérrez Yáñez on 12/12/16.
 */
import React, { Component } from 'react';

class MyComp extends Component {
  render() {
    return (
        <span>HOLA</span>
    );
  }
}

export default MyComp;
