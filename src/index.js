import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import App from './App';
import Column from './Column';
import './index.css';

ReactDOM.render(
  <App title="YEPE" />,
  document.getElementById('root')
);


/*ReactDOM.render( <MuiThemeProvider>
      <Column title="OYAYES"/>
    </MuiThemeProvider>, document.getElementById('root') );*/
