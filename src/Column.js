/**
 * Created by César Gutiérrez Yáñez on 12/12/16.
 */

import React, { Component } from 'react';
import './Column.css';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';

import TextField from 'material-ui/TextField';
import ActionDelete from 'material-ui/svg-icons/action/delete';

import Chip from 'material-ui/Chip';
import MyComp from './MyComp';

class Column extends Component {

  constructor(props) {
    super(props);
    this.state = {
      numChildren: 0,
      itemsList:[],
      newItem: ''
    };
    this.addItemHandler = this.addItemHandler.bind(this);
    this.textChangeHandler = this.textChangeHandler.bind(this);
  }


  render() {
    return (
        <Card className="column">
          <CardHeader title={this.props.title} subtitle={this.props.subtitle}/>
          <List>
            {this.state.itemsList}
          </List>
          <TextField
            ref="myTextField"
            hintText="New Todo Item"
            floatingLabelText="Add a new Todo Item"
            onChange={this.textChangeHandler}
          />
          <FlatButton label="Add..." primary={true} onClick={this.addItemHandler}/>
        </Card>
    );
  }

  addItemHandler = function(){
    //() => this.setState(state => ({numChildren: state.numChildren + 1}))
    if(this.state.newItem != ''){
      this.state.itemsList.push(<ListItem className="listItem" rightIcon={<ActionDelete />} key={this.state.numChildren} primaryText={this.state.newItem} />);
      this.setState(state => ({newItem: ''}));
      this.refs.myTextField.getInputNode().value='';
      this.setState(state => ({numChildren: state.numChildren + 1}));
    }
  }

  textChangeHandler = function (){
    this.setState(state => ({newItem: this.refs.myTextField.getValue()}));
  }

}
//material-ui/svg-icons/action/
export default Column;
